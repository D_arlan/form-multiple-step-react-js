import React, { Component } from 'react';

export default class Nome extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }
    render() {
        return (
            <>
                <div>
                    <h1 className="title">Informe seu nome</h1>
                    <h2 className="subtitle">Subtitle</h2>
                    <div className="field">
                        <div className="control">
                            <input className="input is-medium is-rounded" type="text" />
                        </div>
                    </div>
                    <button
                        className="button is-success is-success is-fullwidth is-medium is-rounded"
                        onClick={this.continue}
                    >
                        <span>Proximo</span>
                        <span className="icon is-small">
                            <i className="fas fa-arrow-right"></i>
                        </span>
                    </button>
                </div>
            </>)
    }
}





