import React, { Component } from 'react';

export default class Obrigado extends Component {


    render() {
        return (<>
            <div>
                <h1 className="title has-text-centered">Obrigado!</h1>
                <h2 className="subtitle has-text-centered">Em breve entraremos em contato</h2>
            </div>
        </>);
    }
}
