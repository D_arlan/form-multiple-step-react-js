import React, { Component } from 'react'
import Nome from '../Nome'
import Telefone from '../Telefone'
import Email from '../Email'
import Obrigado from '../Obrigado'

export default class Main extends Component {
    state = {
        step: 1,
        progresso: 1
    }

    nextStep = () => {
        const { step, progresso } = this.state
        this.setState({
            step: step + 1,
            progresso: progresso + 1
        })
    }
    prevStep = () => {
        const { step, progresso } = this.state
        this.setState({
            step: step - 1,
            progresso: progresso - 1
        })
    }

    showStep = () => {
        const { step } = this.state;
        if (step === 1) {
            return <Nome
                nextStep={this.nextStep}
                prevStep={this.prevStep} />
        }
        if (step === 2) {
            return <Telefone
                nextStep={this.nextStep}
                prevStep={this.prevStep} />
        }
        if (step === 3) {
            return <Email
                nextStep={this.nextStep}
                prevStep={this.prevStep} />
        }
        if (step === 4) {
            return <Obrigado />
        }
    }
    render() {
        return (
            <>
                <progress class="progress is-primary" value={`${this.state.progresso}`} max="4">15%</progress>
                {this.showStep()}
            </>

        )
    }
}