import React, { Component } from 'react';

export default class Email extends Component {

    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }
    voltar = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        return (<>
            <div>
                <h1 className="title">Informe seu Email</h1>
                <h2 className="subtitle">Subtitle</h2>
                <div className="field">
                    <div className="control">
                        <input className="input is-medium is-rounded" type="email" />
                    </div>
                </div>
                <p className="buttons">
                    <button
                        onClick={this.voltar}
                        className="button is-white is-medium is-rounded">
                        <span className="icon is-small">
                            <i className="fas fa-arrow-left"></i>
                        </span>
                        <span>Voltar</span>
                    </button>
                    <button
                        onClick={this.continue}
                        className="button is-success is-success is-medium is-rounded">
                        <span>Proximo</span>
                        <span className="icon is-small">
                            <i className="fas fa-arrow-right"></i>
                        </span>
                    </button>
                </p>

            </div>
        </>);
    }
}
