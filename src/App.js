import React from 'react';
import Main from './components/Main'
import './global.css'

function App() {
  return (
    <div className="App">
      <section class="hero is-fullheight">
        <div class="hero-body">
          <div class="container">
            <div class="columns is-centered">
              <div class="column is-5">
                <Main />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
